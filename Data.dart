class Data {
  String? appName;
  String? catogory;
  int? year;

  Data(this.appName, this.catogory, this.year);

  void convertAppNameToUpperCase() {
    String? name = this.appName;
    print(name!.toUpperCase());
  }

  void display() {
    print("App name: $appName");
    print("Catogory : $catogory");
    print("Year: $year");
  }
}

void main() {
  Data object = new Data("Zulzi", "Best inovative", 2021);
  object.convertAppNameToUpperCase();

  final data1 = Data("takeAlot", "Best Bussiness ", 2020);
  data1.convertAppNameToUpperCase();

  object.display();
  data1.display();
}
